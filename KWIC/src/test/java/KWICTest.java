import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Spliterators;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class KWICTest {
    public ArrayList<String> getTestList(){
        ArrayList<String> ans = new ArrayList<>();
        ans.add("is" );
        ans.add("the" );
        ans.add("of" );
        ans.add("and" );
        ans.add("as" );
        ans.add("a" );
        ans.add("but" );
        ans.add("::" );
        ans.add("Descent of Man" );
        ans.add("The Ascent of Man" );
        ans.add("The Old Man and The Sea" );
        ans.add("A Portrait of The Artist As a Young Man" );
        ans.add("A Man is a Man but Bubblesort IS A DOG" );
        return ans;
    }
    @Test
    void testreadignore() {
        ArrayList<String> ans=getTestList();
        ArrayList<String> expected= new ArrayList<>();
        expected.add("is");
        expected.add("the");
        expected.add("of");
        expected.add("and");
        expected.add("as");
        expected.add("a");
        expected.add("but");

        assertEquals(new KWIC().readignore(ans), expected);
    }

    @Test
    void testreadtitles() {
        ArrayList<String> ans=getTestList();
        ArrayList<String> expected= new ArrayList<>();
        expected.add("Descent of Man" );
        expected.add("The Ascent of Man" );
        expected.add("The Old Man and The Sea" );
        expected.add("A Portrait of The Artist As a Young Man" );
        expected.add("A Man is a Man but Bubblesort IS A DOG" );

        assertEquals(new KWIC().readtitles(ans), expected);
    }

    @Test
    void splitkey(){
        ArrayList<String> ans= new ArrayList<>();
        ans.add("Descent of Man" );
        ans.add("The Ascent of Man" );
        ArrayList<String> expected= new ArrayList<>();
        expected.add("Descent" );
        expected.add("of" );
        expected.add("Man" );
        expected.add("The" );
        expected.add("Ascent" );
        expected.add("of" );
        expected.add("Man" );
        assertEquals(new KWIC().getKey(ans),expected);
    }

    @Test
    void cleankey(){
        ArrayList<String> ans= new ArrayList<>();
        ans.add("Descent" );
        ans.add("of" );
        ans.add("Man" );
        ans.add("The" );
        ans.add("Ascent" );
        ans.add("of" );
        ans.add("Man" );
        ArrayList<String> igs= new ArrayList<>();
        igs.add("of" );
        igs.add("The" );
        ArrayList<String> expected= new ArrayList<>();
        expected.add("Descent" );
        expected.add("Man" );
        expected.add("Ascent" );
        expected.add("Man" );
        assertEquals(new KWIC().cleankey(ans,igs),expected);
    }

    @Test
    void SortUniqueWords(){
        ArrayList<String> ans= new ArrayList<>();
        ans.add("Descent" );
        ans.add("Man" );
        ans.add("Ascent" );
        ans.add("Man" );
        ArrayList<String> expected= new ArrayList<>();
        expected.add("Ascent" );
        expected.add("Descent" );
        expected.add("Man" );
        assertEquals(new KWIC().sortUnikey(ans),expected);
    }

    @Test
    void replaceWord(){
        String k="artist";
        String l="a portrait of the artist as a young man";
        String result= new KWIC().pElement(k,l);
        String expected="a portrait of the ARTIST as a young man";
        assertEquals(result,expected);
    }

    @Test
    void replaceTwoWords(){
        String k="man";
        String l="a man is a man but bubblesort is a dog";
        String result= new KWIC().pElement(k,l);
        String expected="a MAN is a man but bubblesort is a dog"+"\n"
                +"a man is a MAN but bubblesort is a dog"+"\n";
        assertEquals(result,expected);
    }

}