import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Scanner;

import static org.junit.Assert.assertTrue;

public class KWIC {
    public ArrayList<String> readignore(ArrayList<String> all){
        ArrayList<String> first= new ArrayList<String>();

        int a=all.indexOf("::");
        if(a!=-1 && a!=0 && a!=all.size()-1) {
            for (int i = 0; i < all.size() - 1; i++) {
                if(all.get(i)=="::"){
                break;
                }
                first.add(all.get(i));
            }
            return first;
        }
        return null;
    }

    public ArrayList<String> readtitles(ArrayList<String> all) {
        int a = all.indexOf("::");
        ArrayList<String> last = new ArrayList<String>();
        if (a != -1 && a != 0 && a != all.size() - 1) {
            for (int i = a+1; i < all.size() ; i++) {
                if (all.get(i) == " ") {
                    break;
                }
                last.add(all.get(i));
            }
            return last;
        }
        return null;
    }

    public ArrayList<String> getKey(ArrayList<String> title) {
        ArrayList<String> key = new ArrayList<String>();
        for (int i=0;i<title.size();i++){
            String a= title.get(i);
            String c[]=a.split(" ");
            for (int j=0; j<c.length;j++) {
                key.add(c[j]);
            }
        }
        return key;
    }

    public ArrayList<String> cleankey(ArrayList<String> orginalkey,ArrayList<String> ignore) {
        for(int i=0;i<ignore.size();i++){
            String b=ignore.get(i);
            orginalkey.removeIf(s -> s.equalsIgnoreCase(b));
        }
        return orginalkey;
    }

    public ArrayList<String> sortUnikey(ArrayList<String> arr) {

        HashSet<String> set = new HashSet<String>(arr);
        ArrayList<String> tempList = new ArrayList<String>(set);

        ArrayList sortUnikey = new ArrayList(tempList);
        Collections.sort(sortUnikey);
        return sortUnikey;
    }

    public String pElement(String k, String l){
        int count=0;
        int index=0;
        String rtn = "";
        while((index=l.indexOf(k,index))!=-1){
            StringBuilder sb=new StringBuilder();
            sb.append(l);
            String line=l;
            String upper=k.toUpperCase();
            sb.replace(index,index+k.length(),k.toUpperCase());
            System.out.println(sb);
            rtn=rtn+sb.toString()+"\n";
            index=index+k.length();
            count++;
        }
        return rtn;
}

    public static void main(String[] args) {
        ArrayList<String> all = new ArrayList<String>();
        ArrayList<String> ignore = new ArrayList<String>();
        ArrayList<String> title = new ArrayList<String>();

        Scanner scan = new Scanner(System.in);

        while (scan.hasNextLine()) {
            String str = scan.nextLine().toLowerCase();
            if (str.equals("")) {
                break;
            }
            all.add(str);
        }
        scan.close();

        int a = all.indexOf("::");
        if (a == -1 | a == 0 | a == all.size() - 1) {
            System.out.println("input error");
        }

        ignore = new KWIC().readignore(all);
        title = new KWIC().readtitles(all);

        ArrayList<String> keys = new ArrayList<String>();
        keys = new KWIC().getKey(title);

        ArrayList<String> ckey = new ArrayList<String>();
        ckey = new KWIC().cleankey(keys, ignore);

        ArrayList<String> skey = new ArrayList<String>();

        skey = new KWIC().sortUnikey(ckey);

        for (int i=0; i<skey.size();i++){
            String k=skey.get(i);
            for (int j=0; j<title.size();j++){
                String l=title.get(j);
                boolean status= l.contains(k);
                if(status){
                    new KWIC().pElement(k,l);
                }
            }
        }
    }
}
